#第一财经集结号游戏上下分商家微信tcx
#### 介绍
集结号游戏上下分商家微信【溦:5546055】，集结号游戏上下分银商【溦:5546055】，集结号游戏币充值上下分微信【溦:5546055】，集结号游戏金币上下分【溦:5546055】，集结号游戏买分卖分商人【溦:5546055】，/>　　爱好养育花卉多年，若论观花品种，尤以喜爱傲霜的秋菊为甚。逢到天也高肃、云也轻盈、风也潇洒、白露凝霜的重阳时节，便到花市精选几盆“素萼迎寒秀，金英带露香”的菊花搬回家欣赏。　　许是偏爱菊花的缘故，素日读书看报，对有关菊花的新闻、照片、传说、绘画、典故也格外留意。不久前，德州《长河晨刊》编发了一幅菊花照片，见之上介绍德州培育的菊花品种“黄鹤云飞”、“七宝楼台”、“山村”，以其棵矮、叶茂、花大、色艳的特色在全国菊展中夺魁，心中便油然生出菊花不愧为德州市花的自豪。文革时期，对几十首主席诗词倒背如流，当然也记的那首写菊花的《采桑子。重阳》：“人生易老天难老，岁岁重阳，今又重阳，战地黄花分外香。一年一度秋风劲，不似春光，胜似春光，寥廓江天万里霜”。　　黄河流域是菊花的原产地，其栽培史已有三千多年。在古藉《史记。月令篇》中，就有“秋季之月，鞠有黄华”的记载。菊花象征“凛然、坚贞、顽强”，因此国人对它钟爱有加，把菊与梅、兰、竹并称“四君子”，还把菊花与兰花、水仙、菖莆合称“花中四雅”。悠悠千载，上至帝王将相，下至文人墨客，赞菊咏菊的诗词歌赋可谓俯拾皆是，厚重的文化积淀，让节操显明的菊花成为人们心中坚贞不屈的象征与精神寄托。　　东晋的陶渊明，以爱菊种菊赏菊著称，曾在诗中赞誉菊花：“芳菊开林耀，青松冠岩列。怀此贞秀姿，卓为霜下杰”。他的那句“采菊东篱下，悠然见南山”，更是千古流传。其“陶”姓、其“东篱”，也被后来诗人当作菊的隐含与意向。“秋丛绕舍似陶家，遍绕篱边日渐斜。不是花中偏爱菊，此花开尽更无花”。唐朝元稹诗中的陶家，显然是指陶渊明。“眷言东篱下，数株弄秋光。粲粲滋夕露，英英傲晨霜”。宋朝陆游诗中的“东篱”人一见便知是菊的指代。“碧野桥东陶令身，长红小白作芳邻。秋来不用登高去，自有黄花俯就人”。现代诗人臧克家诗中的陶令，也是指的陶渊明。陶渊明辞官归田，以躬耕自给，以诗酒自娱，以种菊自赏，看似悠然闲适，实是他对当时政治黑暗、官场污浊、世风日下的不满与反抗。所以，历代文人也称菊花为“花中隐士”。　　相传原籍曹州冤句（今山东荷泽）的黄巢，幼时聪颖，五岁能吟。在合家以菊为题联句时，替良思未得出句的祖父吟出“堪与百花为总管，自然天赐赭黄衣”。因自古赭黄乃为帝王专用，此句一出，众皆愕然，其父于惊恐中慌忙训斥。祖父打圆场说“孙不知轻重，可令其另拟一首”。黄巢略加思索，脱口吟出：“飒飒西风满院栽，蕊寒香冷蝶难来”。黄巢于唐末响应王仙芝起义，后被推举为领袖，于公元880年攻克东都洛阳称帝，国号大齐，年号金统，应了他“自然天赐赭黄衣”的诗句。　　明代的冯梦龙，在《警世通言》中记载了一则菊花诗的故事。说有天苏轼拜访宰相王安石，偶见桌上咏菊诗：“黄昏风雨打园林，残菊飘零满地金。”自恃才高的苏轼以为“落黄”有误，随手续了句“秋花不比黄花落，说与诗人仔细听。”王安石见后不悦，贬苏轼任黄州团练副使。苏轼一到黄州，果然见当地黄菊瓣落满地似铺金。方自惭自己见识浅陋，后回京专事向宰相王安石认错自纠。　　清代著名文学家蒲松龄，也特别喜爱菊花，有诗为证：“我昔爱菊成菊癖，佳种不惮求千里。”故他把他的菊癖也写入《聊斋志异》中的《黄英》篇中，说菊癖马之才，闻有佳种必购之，千里不惮。此举感动了菊花仙子黄英兄妹，特幻化人形到马家寄居，就以养菊为生而日渐富裕：一年增舍，二年起夏屋，继而廊舍亭园连垣。马之才认为以东篱为市井有辱黄花，黄英之陶兄却笑曰：“自食其力不为贪，贩花为业不为俗。人固不可苟求富，然亦不必务求贫也。”马妻病卒后之才骋娶黄英为妻，黄英仍以售菊为业并渐渐改变了盲目清高的马之才。薄松龄借《黄英》篇告诉世人：君子爱财取之有道，富裕方是高雅的基础。以东篱为市井也能引来八方商贾、滚滚财富。　　《菊谱前序》云：“所宜贵者菊。苗可以采，花可以药，囊可以枕，酿可以饮，所以高人隐士篱畦圃之间，不可一日无此花也。陶渊明植于三径，采于东篱，渑露掇美，泛以忘忧。钟会赋以五美，谓园华高悬，准天极也；纯黄不杂，后土色也；早植晚发，君子德也；冒霜吐颖，象劲直也；杯中体轻，神仙食也。其为所重如此。”人们爱菊，不只它有高洁韵逸的风姿，不只它有延年益寿的药用，更因为它傲霜挺立，凌寒不凋，有“撑住残秋是此花”的凛然之气。难怪曹雪芹在《红楼梦》中借林黛玉之口称赞菊花：“一从陶令评章后，千古高风说到今”。　　农历九月九日重阳节，古人有佩茱萸囊和采摘菊花插戴于鬓发之中的习俗。《乾淳岁时记》载“禁中例于八日作重九排。当于庆瑞殿分列万菊，灿然眩眼。且点菊灯，略如元夕。都人是日饮新酒，泛萸、簪菊，且各以菊糕为馈。”从汉代起，我国就有重阳登高饮酒的习俗。我国古代药学专著《神农本草经》列菊花为上品，称菊“主诸风头眩，肿痛目欲脱……久服利血气，轻身耐老延年。”唐朝诗人孟浩然有“待到重阳节，还来就菊花”的诗句，宋朝的欧阳修盛赞菊花“欲知却老延龄菊，百花摧时始见花”。现代研究表明，菊花确有抗老防衰之效，有很高的药用价值，故而菊花还有“延寿客”之誉。　　北宋刘蒙泉撰写的《菊谱》，成书于徽宗崇宁三年，即公元1104年，是现存最早的菊花专著。书中详细记述了36种菊花的产地、特性、培植、花期、花姿、形态、变异等，是考究宋代以前菊花栽培史的重要文献。　　建国后，北京每年都要举办大型菊展，千种菊花争奇斗艳，引得中外游客流连忘返。从古到今，中华民族赋予菊花以博大的文化内涵。文化产生凝聚力。文化救助精神，所以人说文化是一个民族的根。在了解了这么多关于菊花的诗歌、传说与文学作品等，再看自己抱回家的“赤线金钩”、“二乔斗艳”、“珠玲翠月”、“金丝卷帘”、“朱砂葵龙”的菊花，更觉得它们碧叶丽英，在清霜中冷艳含娇。进而想到以德为重的德州，之所以选迎寒而香的菊花为市花，或许正因菊花有傲霜挺立、凌寒不屈的君子之德的缘故吧！
	42、别受了点伤就一蹶不振，毕竟未来还有许多大风大浪等待着你去乘风破浪。

#### 参与贡献
Fork 本仓库
新建 Feat_xxx 分支
提交代码
新建 Pull Request
#### 特技
使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md
Gitee 官方博客 blog.gitee.com
你可以 https://gitee.com/explore 这个地址来了解 Gitee 上的优秀开源项目
GVP 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
Gitee 官方提供的使用手册 https://gitee.com/help
Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 https://gitee.com/gitee-stars/